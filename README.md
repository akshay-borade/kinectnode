# Kinect-node-app

## Stack

The Kinect node application uses the NodeJS & WEB3 for interacting with ethereum blockchain.

## How to setup your development environment

  - After installing the latest version of NPM and NodeJS. Use command 'npm run development' in project directory.

## How to setup your production environment  
  
  - Install the pm2 node package.

  - Use command 'pm2 run production' for restarting the server.

  - To stop the old pm2 processes, Use command 'cd tools/scripts && app-stop.sh' in the project directory. 

  - Use following command to check pm2 status -

    - pm2 ls (list the apps which're using the pm2)
    - pm2 log app-name 
    - pm2 stop app-name 
    - pm2 flush (for cleraing the old logs)
    - pm2 reload app-name 
    - pm2 restart app-name 

## DATABASE URLs

  - Staging: mongodb://kinectnode:kinectnode123@ds151180.mlab.com:51180/kinectnode

  - Development: mongodb://localhost/kinectnode
  
  - Production: mongodb://localhost/kinectnode

