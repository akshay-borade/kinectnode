const bodyParser = require('body-parser');
const cors = require('cors');
const express = require('express');
const mongoose = require('mongoose');
const logRequest = require('./application-utilities/method-logger');
const settings = require('./config/settings');

const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

console.log('process.env ======>',process.env.NODE_ENV)

global.router = express.Router();

app.use(cors())
app.use(logRequest);
app.use('/', router);
app.get('/', (req, res) => res.send('Welcome to the kinect node!!'));

require('./routes');
require('./models');
require('./jobs');

require('./config/dependency-include.js');
require('./config/configure-db.js')();

const server = app.listen(process.env.PORT || settings['PORT'], () => {
  const host = server.address().address
  const port = server.address().port
  console.log("Example app listening at http://localhost", host, port)
})
