const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UserWalletSchema = new Schema({
  key: {
    type: String,
    required: true,
  },
  walletAddress: {
    type: String,
    required: true
  },
  privateKey: { 
    type: String, 
    required: true 
  },
  createdAt: {
    type: Date
  }
});

const UserWallet = mongoose.model('UserWallet', UserWalletSchema);
module.exports = UserWallet;
