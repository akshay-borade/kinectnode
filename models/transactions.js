const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const TransactionSchema = new Schema({
  key: {
    type: String
  },
  token: {
    type: String,
    required: true
  },
  transactionID: {
    type: String
  },
  nonce: {
    type: String
  },
  attempt: { 
    type: Number, 
    default: 1 
  },
  overviewID: { 
    type: Number,
    required: true  
  },
  walletAddress: {
    type: String,
    required: true
  },
  transactionType: {
    type: String,
    enum : ['TRANSFER', 'MINT']
  },
  status: { 
    type: String, 
    enum : ['INITIATED', 'NOT_INITIATED', 'COMPLETED', 'FAILED'],
    required: true
  },
  createdAt: {
    type: Date
  },
  updatedAt: {
    type: Date
  }
});

const Transaction = mongoose.model('Transaction', TransactionSchema);
module.exports = Transaction;
