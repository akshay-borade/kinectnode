const Transaction = require('../../models/transactions');

const messages = require('../../config/messages');
const {errorOverviewIdLength, errorCompleteTransaction, completeTransactionSuccess} = messages;

const getCompleteTransacData = async (overViewIds, res) => {
  console.log('<======getCompleteTransactionData method called======>');
  const overviewIdLength = overViewIds && overViewIds.length; 
  if (!overviewIdLength) {
    return configSettings.responseHandler(res, null, errorOverviewIdLength, true, 400); 
  }
  const query = {overviewID: {'$in': overViewIds}, status: 'COMPLETED'};
  let completedTransactions;
  try {
    completedTransactions = await Transaction.find(query).exec();  
  } catch(e) {
    console.log('<======complete transaction error=======>', e);
    completedTransactions = false;
  }
  const msg = completedTransactions ? completeTransactionSuccess : errorCompleteTransaction;
  const errorCode = completedTransactions ? 200 : 400;
  return configSettings.responseHandler(res, completedTransactions, msg, completedTransactions === false, errorCode); 
};

module.exports = {getCompleteTransacData}
