const UserWallet = require('../../models/user-wallet');
const methodHelpers = require('./user-wallet-method-helpers');
const messages = require('../../config/messages');

const connectContract = require('../../config/contract');

const {
  userFindError, 
  userAlreadyExist,
  keyGeneration,
  userInsertionError,
  userInsertSuccess,
  userNotExist,
  balanceSuccessMsg,
  contractError
} = messages;

const addUserWallet = (uniqUserId, res) => {
  UserWallet.findOne({key: uniqUserId}, async (err, user) => {
    if (err || user) {
      let msg = err ? userFindError : userAlreadyExist;
      return configSettings.responseHandler(res, null, msg, true, 400);  
    }
    let generateAddress = null;
    try {
      generateAddress = await methodHelpers.generateAddress();  
    } catch(e) {
      console.log('key generation error======>',e)
      return configSettings.responseHandler(res, null, keyGeneration, true, 400);        
    }
    const {privateKey, publicKey} = generateAddress;
    const obj = {
      key: uniqUserId,
      walletAddress: publicKey,
      privateKey: privateKey,
      createdAt: new Date()  
    }
    const newWallet = new UserWallet(obj);
    newWallet.save(obj, (err, res2) => {
      console.log(err)
      let msg = err ? userInsertionError : userInsertSuccess;
      let code = err ? 400 : 200;
      const walletAddress = err ? null : {'walletAddress': publicKey};
      configSettings.responseHandler(res, walletAddress, msg, !!err, code);
    })
  });
};

const getUserBalance = (uniqUserId, res) => {
  UserWallet.findOne({key: uniqUserId}, (err, res1) => {
    if (err || !res1) {
      let msg = err ? userFindError : userNotExist;
      return configSettings.responseHandler(res, null, msg, true, 400);
    }
    const {walletAddress, privateKey} = res1;
    const {connect, contractMethods} = connectContract();
    if (!connect) {
      return configSettings.responseHandler(res, null, contractError, true, 400);
    } 
    // contract method getBalance i.e example - '123452'
    // example - 0xEadEc6DdaF2FE900a0a2b64E688011D1E95c0a9e
    contractMethods.balanceOf(walletAddress, (err, res2) => {
      let msg = err ? contractMethodError : balanceSuccessMsg;
      let obj = err ? null : {balance: res2.toNumber()};
      configSettings.responseHandler(res, obj, msg, !!err, err ? 400: 200);
    });
  });
};

module.exports = {
  addUserWallet,
  getUserBalance
}


