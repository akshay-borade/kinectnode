const UserWallet = require('../../models/user-wallet');

const bip39 = require("bip39");
const bitcore = require("bitcore-lib");
const ethUtil = require("ethereumjs-util");

let mnemonic = bip39.generateMnemonic();
let seed = bip39.mnemonicToSeed(mnemonic).toString("hex");
let hdPrivateKey = bitcore.HDPrivateKey.fromSeed(seed, bitcore.Networks.mainnet);

const methodHelpers = {
  async generateAddress() {
    let userWallets = await UserWallet.find().exec();  
    const totalAccount    = userWallets.length + 1;
    let childHDPrivateKey = hdPrivateKey.derive(`m/44'/60'/${totalAccount}`);
    let rsa = {
      privateKey: ethUtil.addHexPrefix(childHDPrivateKey.privateKey.toString()),
      publicKey: "0x" + ethUtil.privateToAddress(
                ethUtil.toBuffer(
                  ethUtil.addHexPrefix(childHDPrivateKey.privateKey.toString()))
                ).toString("hex")
    };

    let isDublicate = await UserWallet.findOne({'walletAddress': rsa['publicKey']}).exec();  
    if (isDublicate) return methodHelpers.generateAddress();
    else {
      return rsa;
    }
  }
};

module.exports = methodHelpers;
