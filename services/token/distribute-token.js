const UserWallet = require('../../models/user-wallet');
const Transaction = require('../../models/transactions');
const tokenMethodHelper = require('./distribute-token-method-helpers');

const messages = require('../../config/messages');
const {transactionAdded} = messages;

const distributeToken = async (tokenData, res) => {
  let userAddresses; 
  try {
    let userAddresses = await tokenMethodHelper.getUsersAddress(tokenData);
    Transaction.insertMany(userAddresses, (err ,res1) => {
      if (err) {
        throw err;
      } else {
        return configSettings.responseHandler(res, null, transactionAdded, false, 200);     
      }
    })
  } catch(e) {
    console.log('<====distributeToken service error=====>',e)
    return configSettings.responseHandler(res, null, e, true, 400); 
  }
};  

module.exports = {distributeToken}
