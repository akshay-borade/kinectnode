const Transaction = require('../../models/transactions');

const messages = require('../../config/messages');
const connectContract = require('../../config/contract');

const {
  invalidParams,
  contractMethodError,
  mintSuccessfully,
  web3Error,
  invalidWalletAddress,
  mintTransactionAdded,
  mintTransactionInsertionError
} = messages;

const addToken = async (data, res) => {
  const {walletAddress, token, overviewID} = data;
  if (!walletAddress || !token) {
    return configSettings.responseHandler(res, null, invalidParams, true, 400);
  }
  const {connect, contractMethods, web3} = connectContract();
  const isValidAddress = web3 && web3.isAddress(walletAddress);
  if (!web3 || !isValidAddress) {
    const msg = !web3 ? web3Error : invalidWalletAddress;
    return configSettings.responseHandler(res, null, msg, true, 400);
  }

  try {
    const obj = {
      token,
      walletAddress,
      transactionType: 'MINT',
      status: 'NOT_INITIATED',
      createdAt: new Date(),
      overviewID: overviewID
    };
    const transaction = new Transaction(obj);
    const isInserted = await transaction.save(obj);
    if (isInserted) {
      return configSettings.responseHandler(res, null, mintTransactionAdded, false, 200);
    } else {
      throw mintTransactionInsertionError;
    }
  } catch(e) {
    return configSettings.responseHandler(res, null, e.toString(), true, 400);
  }
};

module.exports = {addToken}
