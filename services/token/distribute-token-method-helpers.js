const _ = require('lodash');
const UserWallet = require('../../models/user-wallet');
const messages = require('../../config/messages');
const {invalidAnyUserId, userFindError, contractError} = messages;

const getUsersAddress = (users) => {
  return new Promise((resolve, reject) => {
    const userIds = _.map(users, 'key');
    UserWallet.find({key: {'$in': userIds}}, (err, res) => {
      const anyUserInvalid = res.length !== userIds.length;
      if (err || anyUserInvalid) {
        const msg = err ? userFindError: invalidAnyUserId; 
        return reject(msg);
      } 
      users = _.map(users, (user, index) => {
        user['walletAddress'] = res[index].walletAddress;
        user['createdAt'] = new Date();
        user['status'] = 'NOT_INITIATED';
        user['transactionType'] = 'TRANSFER';
        return user;
      })
      return resolve(users);
    });
  });
};

module.exports = {getUsersAddress}

