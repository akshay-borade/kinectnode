const Web3 = require('web3');
const settings = require('./settings.js');

module.exports = () => {
  const {contractAddress, contractABI, web3Provider} = settings;
  let contractData = {}, contractMethods, web3;
  try {
    web3 = new Web3(new Web3.providers.HttpProvider(web3Provider)); 
    const kinectContract = web3.eth.contract(contractABI);
    contractMethods = kinectContract.at(contractAddress);
  } catch(e) {
    console.log('catch block -======>')
    contractData['connect'] = false;
  } 
  if (!contractData.hasOwnProperty['connect']) {
    contractData = {
      connect: true,
      contractMethods,
      web3
    }
  } 
  return contractData;
};

