const responseHandler = require('../application-utilities/response-handler.js');
const authorizeUser = require('../application-utilities/middlewares.js');

global.configSettings = {responseHandler, authorizeUser};

module.exports = configSettings;
