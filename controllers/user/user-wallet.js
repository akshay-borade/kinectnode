
const userWalletService = require('../../services/user-wallet/user-wallet.js');
const distributeTokenService = require('../../services/token/distribute-token.js');
const addTokenService = require('../../services/token/add-token.js');
const getCompleteTransactionsService = require('../../services/complete-transactions/get-complete-transactions.js');

const messages = require('../../config/messages');
const {invalidParams} = messages;

const addUserWallet = (req, res) => {
  const uniqUserId = req.body['id'];
  userWalletService.addUserWallet(uniqUserId, res);
};

const getUserBalance = (req, res) => {
  const uniqUserId = req.params.userId;
  userWalletService.getUserBalance(uniqUserId, res);
};

const addToken = (req, res) => {
  const data = req.body;
  addTokenService.addToken(data, res);
};

const distributeToken = (req, res) => {
  const users = req.body.users;
  let parseUsers;
  try {
    parseUsers = JSON.parse(users);
  } catch(e) {
    return configSettings.responseHandler(res, null, invalidParams, true, 400); 
  }
  distributeTokenService.distributeToken(parseUsers, res);
};

const getCompleteTransactions = (req, res) => {
  let overViewIds = req.body.ids;
  try {
    overViewIds = JSON.parse(overViewIds);
  } catch(e) {
    return configSettings.responseHandler(res, null, invalidParams, true, 400); 
  }
  getCompleteTransactionsService.getCompleteTransacData(overViewIds, res);
};

module.exports = {
  addUserWallet,
  getUserBalance,
  getCompleteTransactions,
  distributeToken,
  addToken  
};
