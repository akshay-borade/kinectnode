const userController = require('../controllers/user/user-wallet.js');
const configSettings = require('../config/dependency-include');

module.exports.route = () => {
  router.post('/addUserWallet', configSettings.authorizeUser, userController.addUserWallet);
  router.post('/distributeToken', configSettings.authorizeUser, userController.distributeToken);

  router.post('/addToken', configSettings.authorizeUser, userController.addToken);
  router.post('/getCompletedTransactions', configSettings.authorizeUser, userController.getCompleteTransactions);

  router.get('/getUserBalance/:userId', configSettings.authorizeUser, userController.getUserBalance);
};


