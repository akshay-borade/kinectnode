echo "Wait...Running PM2 is in progress"

clear

echo "Removing the previous processes and logs..."


pm2 flush && pm2 stop all 2>/dev/null

processFound=$?

echo "processFound ======> $processFound"

command1Result=0

if [ $processFound -ne 0 ] ; then
  echo -e "\nThere is no running process to delete."
else
  echo "Running process found & process deleting started"
  pm2 delete all 2>/dev/null
  command1Result=$?
fi

echo "commnad result 1 ======> $command1Result"
if [ $command1Result -ne 0 ] ; then
  echo -e "\nPM2 command failed to stop all the previous processes."
  echo -e "Aborting...\n"
  exit 105
else
  echo "Starting Server..."  
fi

pm2 start ../server-start.json --env production

command2Result=$?

echo "commnad result 2 ======> $command2Result"

if [ $command2Result -ne 0 ] ; then
  echo -e "\nPM2 command failed to start the server."
  echo -e "Aborting...\n"
  exit 105
fi

echo "Script execution has been completed"

echo "Have a nice day!!"
