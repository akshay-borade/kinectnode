echo "Stop PM2 process....."

clear

echo "Clearing and removing logs..."

pm2 flush && pm2 stop all && pm2 delete all 2>/dev/null

command1Result=$?

echo "commnad result 1 ======> $command1Result"
if [ $command1Result -ne 0 ] ; then
  echo -e "\nPM2 command failed to stop all the previous processes."
  echo -e "\nIt may be possible that no one process is there to stop."
  echo -e "Aborting...\n"
  exit 105
else
  echo "In Else command 1"  
fi

echo "Script execution has been completed!!"
echo "Have a nice day!!"
