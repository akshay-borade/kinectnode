const expect = require('chai').expect;
const mongoose = require('mongoose');

const connectDB = require('./method-helpers/db-connection');
const dbURL = process.env.MONGO_URL;

describe('DB connection',function () {
  this.timeout(6000);
  it('DB connect', async function () {
    let result = false;
    try {
      result = await connectDB(dbURL);  
    } catch(e) {
      console.log('catch exception!!');
    } 
    expect(result).to.equal(true);  
  });  
});
