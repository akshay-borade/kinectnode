const expect = require('chai').expect;
const mongoose = require('mongoose');

const settings = require('../config/settings');
const {railsProductionURL} = settings;

const appURL = process.env.railsAppURL;

describe('Rails Application URL',function () {
  it('Check URL', function () {
    const isValid = (appURL === railsProductionURL);
    expect(isValid).to.equal(true);  
  });  
});
