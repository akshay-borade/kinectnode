const mongoose = require('mongoose');

module.exports = (url) => {
  return new Promise((resolve, reject) => {
      const dbConnection = mongoose.connect(url).catch((err) => reject(false));
      mongoose.connection.on('connected', (res) => resolve(true));
      mongoose.connection.on('error', (err) => reject(false));
  });
};
