const settings = require('../config/settings');
const notAuthorizeUser = require('../config/messages').notAuthorizeUser;

const {authorizeCode} = settings;

const sendError = (res) => {
  return configSettings.responseHandler(res, null, notAuthorizeUser, true, 401);    
}

module.exports = (req, res, next) => {
  const authHeader = req.headers['authorization'];
  try {
    const token = authHeader && (authHeader.split(' ')[1] === authorizeCode);  
    if (!authHeader || !token) return sendError(res);    
    next();
  } catch(e) {
    console.log('error in middleware =====>',e)    
    return sendError(res);
  }
}

