
module.exports = (req, res, next) => {
  console.log('<====LOGGED REQUEST START=====>', new Date());
  const {body, method, params, baseUrl, originalUrl} = req;
  console.log('ORIGINAL URL =====>', originalUrl)
  console.log('METHOD =====>', method);
  console.log('BODY =====>', body);
  console.log('PARAMS ====>', params);
  console.log('<====LOGGED REQUEST END=====>');
  next();
};
