const CronJob = require('cron').CronJob;
const transactionQueueHelper = require('./method-helpers/transaction-queue-helper');
/***
  * Run job at 
  * Timezone - 'Asia/Calcutta'
  * CronTime - '/20 * * * * *' 
***/  

// '0/20 * * * * *'

const transactionQueue = new CronJob({
  cronTime: '*/20 * * * * *', 
  onTick: function() {
    console.log('<=============Perform Job==========>',new Date());
    transactionQueueHelper();
  },
  start: false,
  timeZone: 'Asia/Calcutta'
});

transactionQueue.start();

