const Transaction = require('../../models/transactions');
const Web3 = require('web3');
const moment = require('moment');
const settings = require('../../config/settings.js');
const errorMessage = require('../../config/messages.js');

const rp = require('request-promise');

const {
  transactionPendingMessage, 
  failedTransactionStart, 
  exceptionUpdatingTransacStatus,
  errorWhileSendingDataToRails
} = errorMessage;

const {
  railsAppURL, 
  web3Provider, 
  railsProductionURL
} = settings;

const web3 = new Web3(new Web3.providers.HttpProvider(settings.web3Provider));


const addTransactionIntoFailed = async (transaction) => {
  const {_id, status} = transaction;
  let isStatusUpdated;
  console.log('<==============addTransactionIntoFailed started ====>');
  try {
    isStatusUpdated = await Transaction.update({'_id': _id}, {'status': 'FAILED', 'attempt': 3}).exec();
  } catch(e) {
    console.log('<==============addTransactionIntoFailed error ====>', e);
  }
  return isStatusUpdated;
};

const checkTransaction = async (transaction) => {
  const {
    transactionID, 
    key, 
    token, 
    _id, 
    overviewID, 
    transactionType, 
    walletAddress,
    createdAt
  } = transaction;
  
  let txResponse;

  try {
    txResponse = await web3.eth.getTransactionReceipt(transactionID);
  } catch(e) {
    console.log('<=====Error while fetching the transaction Receipt==========>');
    data = {'msg': `${errorWhileFetchingtheTransacReceipt} ===> ${e}`};
  }
  if (txResponse) {
    const txResponseStatus = parseInt(txResponse.status);
    const status = txResponseStatus ? 'COMPLETED' : 'FAILED';
    let updated, data;
    try {
      updated = await Transaction.update({'_id': _id}, {'status': status, 'updatedAt': new Date()}).exec();
      if (txResponseStatus) {
        const railsAppTranxStatus = await sendTransactionOnRailsSide(key, token, status, transactionID, overviewID, transactionType, walletAddress);
        data = (railsAppTranxStatus && railsAppTranxStatus['msg']) ? railsAppTranxStatus : true;
      } else {
        console.log('<====Failed Fransaction Status 0 From getTransactionReceipt======>');
        data = {'msg': failedTransactionStart}
      }
    } catch(e) {
      console.log('<====Exception while updating the transaction status======>');
      data = {'msg': `${exceptionUpdatingTransacStatus} ===> ${e}`};
    }
    return data;
  } else {
    const isBlockCreated = await web3.eth.getTransaction(transactionID);
    console.log('isBlockCreated ========>',isBlockCreated);
    const oldTime =  moment(createdAt);
    const newTime =  moment(); 
    const duration = moment.duration(newTime.diff(oldTime));
    const seconds = duration.asSeconds();  
    let isStatusUpdated;
    if (!isBlockCreated && seconds >= 150) {
      console.log('<=============== Block is not created ===========>', seconds);
      isStatusUpdated = await addTransactionIntoFailed(transaction);
    }
    return isStatusUpdated ? true : {'msg': transactionPendingMessage};
  }
};

const sendTransactionOnRailsSide = async (key, token, status, transactionID, overviewID, transactionType, walletAddress) => {
  let railsSideTransaction;
  let appURL = process.env.railsAppURL || railsAppURL;
  let isDataSendToRailsAPP = false;
  try {
    const options = {
      method: 'POST',
      uri: appURL + '/dashboard/transaction_callback',
      body: {key, token, status, transactionID, overviewID, transactionType, walletAddress},
      json: true
    };
    console.log('<======URL Rails Side ===========>', options['uri'])
    railsSideTransaction = await rp(options);
    console.log('<=====Send data successfully to rails side=====>')
    console.log('transactionType ======>',transactionType);
    console.log(key, token, status, transactionID, overviewID, transactionType);
    isDataSendToRailsAPP =  true;
  } catch(e) {
    console.log('<=====Error while sending data to rails side=====>')
    isDataSendToRailsAPP = {'msg' : `${errorWhileSendingDataToRails} ===> ${e}`}
  }
  return isDataSendToRailsAPP;
};


/***
  * Exception cases -
    * When web3 transactionReceipt API not working
    * When updating the transaction status
    * Failed Fransaction Status 0 From getTransactionReceipt
    * When sending data to rails side, In case API not works
    * When transaction not mined yet (pending status)
    * When transaction is pending
***/  

module.exports = async (transaction) => {
  let transactionData;
  try {
    transactionData = await checkTransaction(transaction);
    if (transactionData && transactionData['msg']) throw transactionData['msg'];
    if (!transactionData) throw 'unspecified reason, It may be because of system'

  } catch(e) {
    console.log('error while performing transaction ', e)
  }
};
