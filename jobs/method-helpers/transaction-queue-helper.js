const Transaction = require('../../models/transactions');
const performTransaction = require('./perform-transaction');
const checkTransaction = require('./check-transaction');
const initiateFailedTransaction = require('./initiate-failed-transaction');

const getFirstEntryFromQueue = async () => {
  let transaction;  
  try {
    let query = Transaction.find({status: {'$ne': 'COMPLETED'}}).sort({'_id': 1}).limit(1);
    transaction = await query.exec();
  } catch(e) {
    // exception raised    
    console.log('<==== Error while finding the transaction ======>',e);
  }
  return (transaction && transaction.length) ? transaction[0] : null;
};


const logMessage = (transaction) => {
  let msg;
  console.log('<======== Transaction Process Started ==========>')
  if (!transaction) msg = 'No transaction found!!';
  else {
    msg = '<======== Got First Transaction ==========>' + transaction;
  }
  console.log(msg);
};


const processTransaction = async () => {
  // fetch first transaction
  const firstTransaction = await getFirstEntryFromQueue();
  logMessage(firstTransaction);
  if (!firstTransaction) return;
  const {status} = firstTransaction;
  if (status == 'NOT_INITIATED') performTransaction(firstTransaction);
  else if (status == 'INITIATED') checkTransaction(firstTransaction);
  else {
    console.log('<=============Initiated failed transaction===============>');
    initiateFailedTransaction(firstTransaction);
  }
};

module.exports = () => processTransaction()


