
module.exports = (contractMethods, toAddress, token, transaction) => {
  const {transactionType} = transaction;
  const isTransfer = ((transactionType === 'TRANSFER') || !transactionType);
  const methodType = isTransfer ? 'transfer' : 'mint';
  console.log('methodType', methodType)
  return contractMethods[`${methodType}`].getData(toAddress, parseInt(token)*1e18);
};
