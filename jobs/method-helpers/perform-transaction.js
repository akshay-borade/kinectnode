const Transaction = require('../../models/transactions');
const Tx = require('ethereumjs-tx');
const Web3 = require('web3');

const connectContract = require('../../config/contract');
const messages = require('../../config/messages');
const config = require('../../config/settings');

const getMintTransferData = require('./get-mint-transfer-data');

const {contractPrivateKey, contractPublicKey, web3Provider, gasLimit, gasPrice, contractAddress} = config;
const privateKey = new Buffer(contractPrivateKey, 'hex');
const fromAddress = contractPublicKey;
const {invalidAnyUserId, userFindError, contractError} = messages;

const updateTransactionStatus = async (transaction, failed) => {
  const {key, transactionId, nonce, _id} = transaction;
  let status = failed ? 'FAILED' : 'INITIATED';
  const updateFields = {'transactionID': transactionId, 'status': status, nonce};
  let updated;
  try {
    updated = await Transaction.update({'_id': _id}, updateFields).exec();  
  } catch(e) {
    console.log('<==== Error while updating the transaction ======>',e);
  }
  return updated;
};

// process transaction 
const sendSigned = (txData, cb) => {
  const transaction = new Tx(txData);
  transaction.sign(privateKey)
  const serializedTx = transaction.serialize().toString('hex')
  const {connect, contractMethods, web3} = connectContract();
  if (!connect) return cb(contractError, null);
  web3.eth.sendRawTransaction('0x' + serializedTx, cb)
};

const generatedTransactionAddress = (transaction) => {
  const {walletAddress: toAddress, token, key, nonce, _id} = transaction;
  return new Promise((resolve, reject) => {
    const {connect, contractMethods, web3} = connectContract();
    if (!connect) return reject(contractError);
    web3.eth.getTransactionCount(fromAddress, (err, txCount) => {
      if (err) return reject(err);
      // contract method
      let currentGasPrice = web3.eth.gasPrice.toNumber();
      currentGasPrice = (currentGasPrice/(10**9));
      if (currentGasPrice < 10) {
        currentGasPrice = 10 * (10**9);        
      }
      const data = getMintTransferData(contractMethods, toAddress, token, transaction);
      const txData = {
        nonce: web3.toHex(txCount),
        gasLimit,
        gasPrice: web3.toHex(currentGasPrice), 
        to: contractAddress,
        from: fromAddress,
        value: 0,
        data
      };
      sendSigned(txData, (err, result) => {
        if (err) return reject({err, transactionFailed: true, nonce: txCount, _id});
        return resolve({transactionId: result, key, nonce: txCount, _id});
      })
    });
  });
};

module.exports = async (transaction) => {
  let transactionData;
  try {
    transactionData = await generatedTransactionAddress(transaction);  
    if (transactionData && transactionData.transactionId) {
      await updateTransactionStatus(transactionData);
    }
  } catch(e) {
    let err = e;
    if (typeof e == 'object' && e.transactionFailed == true) {
      err = e.err;
      try {
        transaction['nonce'] = e.nonce;
        await updateTransactionStatus(transaction, true);  
      } catch(e2) {
        console.log('error while updating the failed status ', e2)        
      }
    }
    console.log('error while performing transaction ', err)    
  }
};
